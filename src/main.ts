import { createApp } from 'vue'
import App from './App.vue'

import './assets/main.css'

const valueToHex = (c:number) => {
  return c.toString(16).toUpperCase()
}

export function RGBToHSL(r:number, g:number, b:number) {
  // Make r, g, and b fractions of 1
  r /= 255;
  g /= 255;
  b /= 255;

  // Find greatest and smallest channel values
  let cmin = Math.min(r,g,b),
      cmax = Math.max(r,g,b),
      delta = cmax - cmin,
      h = 0,
      s = 0,
      l = 0;
  // Calculate hue
  // No difference
  if (delta == 0)
    h = 0;
  // Red is max
  else if (cmax == r)
    h = ((g - b) / delta) % 6;
  // Green is max
  else if (cmax == g)
    h = (b - r) / delta + 2;
  // Blue is max
  else
    h = (r - g) / delta + 4;

  h = Math.round(h * 60);
    
  // Make negative hues positive behind 360°
  if (h < 0)
    h += 360;
  // Calculate lightness
  l = (cmax + cmin) / 2;

  // Calculate saturation
  s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
    
  // Multiply l and s by 100
  s = +(s * 100).toFixed(0);
  l = +(l * 100).toFixed(0);

  return [h, s, l];
  // return "hsl(" + h + "," + s + "%," + l + "%)";
}

export function HSLToRGB(h:number, s:number, l:number){
  h /= 360
  s /= 100
  l /= 100
  var r, g, b;

  if(s == 0){
      r = g = b = l; // achromatic
  } else {
    var hue2rgb = function hue2rgb(p:number, q:number, t:number){
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}

export function RGBToHEX(r:number, g:number, b:number) {

  return '#' + valueToHex(r) + valueToHex(g) + valueToHex(b)

}

createApp(App).mount('#app')
